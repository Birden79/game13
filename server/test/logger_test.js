const { describe, it } = require('mocha');
const { expect } = require('chai');

const logger = require('../src/logger');

describe('Test for logger', () => {
    it('logging should not fail with error', () => {
        expect(logger.print).to.not.throw();
    });
});
