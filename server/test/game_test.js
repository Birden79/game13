const { describe, it } = require('mocha');
const { expect } = require('chai');
const Game13 = require('../src/game13');

const game = new Game13(0, true);

describe('Test for class Game13', () => {
    it('Game.map must be array with (4 >= size <= 16) ', () => {
        for (let i = 0; i < 20; i++) {
            game.Restart(i);
            expect(game.map).to.be.a('array');
            expect(game.map[0]).to.be.a('array');
            expect(game.size).to.gte(4);
            expect(game.size).to.lte(16);
        }
    });
    it('Game.map must contains numbers range [curMin..curMax]', () => {
        for (let x = 0; x < game.size; x++) {
            for (let y = 0; y < game.size; y++) {
                const cell = game.getCell(x, y);
                expect(cell).to.gte(game.curMin);
                expect(cell).to.lte(game.curMax);
            }
        }
    });
    it('Game.getRandom() must return number in range [curMin..curMax]', () => {
        for (let i = 0; i < 100; i++) {
            const num = game.getRandom();
            expect(num).to.gte(game.curMin);
            expect(num).to.lte(game.curMax);
        }
    });
    it('Game.Status() must return game status in JSON format', () => {
        const gameStatus = JSON.parse(game.Status());
        expect(gameStatus.id).to.equal('map');
        expect(gameStatus.data).to.be.a('array');
        expect(gameStatus.score).to.be.a('number');
        expect(gameStatus.gameOver).to.be.a('boolean');
    });
});
