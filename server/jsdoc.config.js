module.exports = {
    plugins: ['plugins/markdown'],
    recurseDepth: 2,
    source: {
        include: ['src/'],
        includePattern: '.+\\.js$',
        excludePattern: '(^|\\/|\\\\)_',
    },
    sourceType: 'module',
    tags: {
        allowUnknownTags: true,
        dictionaries: ['jsdoc', 'closure'],
    },
    templates: {
        cleverLinks: false,
        monospaceLinks: false,
    },
    opts: {
        destination: 'docs/',
    },
};
