const today = new Date().toLocaleString();
/**
 * Simply loger to console
 * @param {string} msg - message for log
 */
exports.print = (msg) => {
    console.log(`${today} > ${msg}`);
};
