/**
 * @fileoverview Game13 - simple game
 * @author Denis Biryukov
 * @version 1.0.0
 */
/**
 * Websocket message format (JSON):
 * @example
 * { id: 'new' }            // for start new game:
 * { id: 'click', x, y }    // on click to map
 * { id: 'undo' }           // for undo game step
 *
 * Return answer in JSON format:
 * @example
 * {
 *  id: 'map',
 *  data: [],
 *  score: number,
 *  gameOver: boolean
 * }
 */

const WebSocket = require('ws');
const express = require('express');
const minimist = require('minimist');
const path = require('path');
const Game = require('./game13');
const logger = require('./logger');

const app = express();

const MAX_CONNECTIONS = 20;
const DEFAULT_GAME_SIZE = 4;
const clientPath = path.join(__dirname, '..//..', 'client');

let connectionCount = 0;

const args = minimist(process.argv.slice(2), {
    default: {
        addr: '127.0.0.1',
        port: '8000',
    },
});

const httpPort = args.port;

app.use(express.static(clientPath));

app.use('/', (req) => {
    logger.print(`Req:${req.method} ${req.url}`);
});

app.listen(httpPort, () => {
    logger.print(`Start listening on port ${httpPort}`);
});

const wsServer = new WebSocket.Server({
    port: 2012,
});


wsServer.on('connection', (ws) => {
    let game;
    if (connectionCount < MAX_CONNECTIONS) {
        connectionCount++;
        logger.print(`Connection #${connectionCount} created`);
        game = new Game(DEFAULT_GAME_SIZE);
        logger.print('New game created');
    }
    ws.on('message', (message) => {
        logger.print(`From client: ${message}`);
        const req = JSON.parse(message);

        if (req.id === 'click') {
            logger.print(`Click on ${req.x},${req.y}`);
            game.Click(req.x, req.y);
            logger.print(`Min:${game.curMin} max:${game.curMax}`);
        } else if (req.id === 'new') {
            logger.print(`Start new game with size ${req.size}`);
            game.Restart(req.size);
        } else if (req.id === 'undo' && !game.gameOver) {
            logger.print('Restore game status');
            game.Undo();
        }
        ws.send(game.Status());
    });
    ws.on('close', () => {
        logger.print('Connection closed');
        connectionCount--;
    });
});
