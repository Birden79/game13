
// eslint-disable-next-line import/no-cycle
import { StartNewGame, SetGameStatus, UpdateGame } from './index';

const CMD_UNDO = JSON.stringify({ id: 'undo' });
const serverAddress = `ws://${window.location.hostname}:2012`;
const socket = new WebSocket(serverAddress);

const logger = console.log; // eslint-disable-line no-console
/**
 *  Called on websocket is open
 *  starts new game
 */
socket.onopen = () => {
    StartNewGame();
};
/**
 *  Called when incoming message received
 */
socket.onmessage = (event) => {
    const gameData = JSON.parse(event.data);
    if (gameData.id === 'map') {
        logger(`Receieved map data: size:${gameData.data[0].length}, score: ${gameData.score}`);
        SetGameStatus(gameData);
        UpdateGame();
    }
};
/**
 * Send click coordinate to game server
 * @param {*} x x coord. of click
 * @param {*} y y coord. of click
 */
export function SendGameEventClick(x, y) {
    logger(`Click on ${x},${y}`);
    socket.send(JSON.stringify({
        id: 'click',
        x,
        y,
    }));
}
/**
 * Send request to game server for create new game session
 * @param {*} size - game size
 */
export function SendGameNew(size) {
    socket.send(JSON.stringify({ id: 'new', size }));
}
/**
 *  Send 'Undo' command to game server
 */
export function SendGameUndo() {
    socket.send(CMD_UNDO);
    logger('Undo step');
}
