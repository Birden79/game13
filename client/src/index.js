
// eslint-disable-next-line import/no-cycle
import { SendGameEventClick, SendGameNew, SendGameUndo } from './proxy';

require('../css/styles.css');

const MAX_CELL_SIZE = 80;
const DEFAULT_MAP_SIZE = 4;
const MAX_MAP_SIZE = 16;
const MIN_MAP_SIZE = 4;
const CANVAS_WIDTH = 800;

const cellFontColor = '#000000';
const cellColors = [
    '#ffffff', '#eee34e', '#73ee4e', '#b17b2a', '#d84b13', '#c9367f', '#9a32ca',
    '#4b1b4b', '#404040', '#1b314b', '#23471a', '#491f03', '#b0b009', '#ffa030'];

const canvas = document.getElementById('gameCanvas');
const textScore = document.getElementById('score');
const textMapSize = document.getElementById('size');
const buttonNew = document.getElementById('new');
const buttonUndo = document.getElementById('undo');
const buttonMapSize = document.getElementById('size');
const ctx = canvas.getContext('2d');

const map = {
    size: 0,
    data: [],
    oldData: [],
    getCell: (x, y) => map.data[y][x],
    getOldCell: (x, y) => map.oldData[y][x],
    setOldCell: (x, y, cell) => { map.oldData[y][x] = cell; },
};

let cellSize;
let cellFontSize;
let dx; let
    dy;
let gameScore; let
    isGameOver;

let mapSize = DEFAULT_MAP_SIZE;

/**
 * Setting up game status
 * @param {*} data - game status object, received from server
 */
export function SetGameStatus(data) {
    let newGame = false;
    map.data = data.data;
    if (map.data[0] !== map.size) { newGame = true; }
    map.size = map.data[0].length;
    gameScore = data.score;
    isGameOver = data.gameOver;
    if (newGame) { // First map data loaded:
        cellSize = Math.floor(CANVAS_WIDTH / map.size);
        cellSize = (cellSize > MAX_CELL_SIZE) ? MAX_CELL_SIZE : cellSize;
        canvas.width = cellSize * map.size;
        canvas.height = cellSize * map.size;
        cellFontSize = Math.floor(cellSize / 2); // - calculate font size for cell
        dx = Math.floor(cellSize / 3); // - calculate text position in cell
        dy = Math.floor(cellSize * 2 / 3);
        for (let i = 0; i < map.size; i++) { map.oldData[i] = []; }
    }
}
/**
 *  Draws a cell of map
 * @param {number} x X coordinate cell
 * @param {number} y Y coordinate cell
 * @param {number} cell cell value
 */
function DrawCell(x, y, cell) {
    const dxx = (cell < 10) ? dx : dx * 0.5;
    ctx.clearRect(x * cellSize, y * cellSize, cellSize, cellSize);
    ctx.fillStyle = cellColors[cell];
    ctx.fillRect(x * cellSize + 1, y * cellSize + 1, cellSize - 2, cellSize - 2);
    ctx.fillStyle = cellFontColor;
    ctx.fillText(cell, x * cellSize + dxx, y * cellSize + dy);
}
/**
 *  Draws a game map
 */
function DrawMap() {
    ctx.font = `${cellFontSize}px arial`;
    for (let x = 0; x < map.size; x++) {
        for (let y = 0; y < map.size; y++) {
            const cell = map.getCell(x, y);
            if (cell !== map.getOldCell(x, y)) { DrawCell(x, y, cell); }
            map.setOldCell(x, y, cell);
        }
    }
}
/**
 *  Update "Map size" value
 */
function ShowMapSize() {
    textMapSize.innerText = `Map size: ${mapSize}`;
}
/**
 *  Update "Score" value
 */
function ShowScore() {
    const scoreText = `Score: ${gameScore}`;
    if (isGameOver) {
        textScore.className = 'sc2';
        textScore.innerText = `Game over (${scoreText})`;
    } else {
        textScore.className = 'sc';
        textScore.innerText = scoreText;
    }
}
/**
 * Change game map size
 */
function ChangeMapSize() {
    if (++mapSize > MAX_MAP_SIZE) { mapSize = MIN_MAP_SIZE; }
}
/**
 *  Start new game session
 */
export function StartNewGame() {
    SendGameNew(mapSize);
}
/**
 *  Called when user clicked on game map
 */
canvas.onclick = (e) => {
    const clickX = Math.floor(e.layerX / cellSize);
    const clickY = Math.floor(e.layerY / cellSize);
    if (!isGameOver) {
        SendGameEventClick(clickX, clickY);
    }
};
/**
 *  Called when user click on button "New game"
 */
buttonNew.onclick = () => {
    StartNewGame(mapSize);
};
/**
 *  Called when user click on button "Undo"
 */
buttonUndo.onclick = () => {
    SendGameUndo();
};
/**
 *  Called when user click on button "Map size"
 */
buttonMapSize.onclick = () => {
    ChangeMapSize();
    StartNewGame(mapSize);
};
/**
 *      Update game state (redraw map, score and map size information)
 */
export function UpdateGame() {
    DrawMap();
    ShowScore();
    ShowMapSize();
}
