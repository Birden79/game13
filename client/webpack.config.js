module.exports = {
    context: __dirname,
    devtool: 'source-client',
    entry: './src/index.js',
    mode: 'production',
    output: {
        path: `${__dirname}/dist`,
        filename: 'main.js',
    },
    module: {
        rules: [{
            test: /\.css$/i,
            use: ['style-loader', 'css-loader'],
        },
        ],
    },
};
